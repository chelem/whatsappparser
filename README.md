# The Whatsapp Conversation Parser

## Background
This project contains a Python script that processes an exported Whatsapp conversation and creates an Excel file 
with the following columns:

Date, Time, Author, Message, Word Count.

The script was develeoped by [The Research Software Company](http://www.chelem.co.il) for 
[Neta Kligler Vilenchik](http://www.netakv.com/) of the Hebrew University in Jeruslem. 
 

## Usage
This script receives an export file from WhatsApp conversation and creates an excel file from it.

In order to use the script, do the following steps:

1.Install the requirements 
```
pip install -r requirements.txt
```

2.Run the script with the input and output files
```
python ParseConversation2.py input_file_name.txt output_file_name.xls 
```
The default date format is 'MM/DD/YY' but if you have another date format you can use the following command

```
python ParseConversation2.py input_file_name.txt output_file_name.xls --date-format DD.MM.YYYY*

 *your date format
```

## Packaging

In case  you want to create .exe file from the script you should do the  following steps:

1.Install pyinstaller 
```
pip install pyinstaller
```

2.Run pyinstaller with .py script as a param
```
pyinstaller ParseConversation2.py
```

The command pyinstaller creates .spec file for the ParseConversation2.py script.
In addition the command creates two folders "dist" and "build"

3.Run pyinstaller with the .spec file
```
pyinstaller ParseConversation2.spec
```

The command creates the .exe file.
If you want to send the program to other users you should send them __all__ the folder dist (not just the .exe file)

4.Run the script - run .exe file of the script. 

Please note that the input file name should contain the path of the file if the file folder is not the same folder as ParseConversation2.exe
```
ParseConversation2.exe input_file_name.txt output_file_name.xls 
```

The default date format is 'MM/DD/YY' but is you have another date format you can use the following comand

```
ParseConversation2.exe input_file_name.txt output_file_name.xls --date-format DD.MM.YYYY*

 * your date format
```

## Use, questions, modifications and improvement
The script is provided under the 3-clause BSD license.
We would appreciate knowing if you use this script and for what purpose. You may contact Itay Zandbank from 
The Research Software Company at [itay@chelem.co.il](mailto:itay@chelem.co.il) .  

## License
Copyright (c) 2017, Neta Kligler Vilenchik.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.