# This script file was developed by Or Barak
# This script can be used and distributed freely without consent from the owner
# This script can be altered as desired without consent from the owner, with the exception of script printouts
# Alteration of script printouts can be done only with written consent from the owner - contact at efor001@gmail.com
# Questions and/or Change Requests for this script will be taken only regarding original script without alterations made by user(s)
# The owner is not responsible for output integrity, accuracy or validity of this script.

import os
import sys
import re
import logging
from openpyxl import load_workbook
from openpyxl import Workbook
from datetime import date
from openpyxl.worksheet import dimensions
from openpyxl.styles import Alignment, Font
from openpyxl.cell import Cell

dictionary = {}  # key=word, value=number of times used
todayDictionary = {}  # key=word, value=number of times used today
dateDictionary = {}  # key=dateStr, value=messages on that date
hourDictionary = {}  # key=hourStr, value=messages on that hour today
hourAccDictionary = {}  # key=hourStr, value=messages on that hour
numberMsgDictionary = {}  # key=phone, value=messages by user
todayNumberMsgDictionary = {}  # key=phone, value=messages by user today
numberWordDictionary = {}  # key=phone, value=words by user
todayNumberWordDictionary = {}  # key=phone, value=words by user today
userDailyCounterDictionary = {}  # key1=phone, key2=day of the week, value=messages by user on given day
dailyCounterDictionary = {}  # key=day of the week, value=messages on given day
userHourCounterDictionary = {}  # key1=phone, key2=hour of the day, value=messages by user on given hour
userMessageCounterDictionary = {}  # key1=phone, key2=date, value=messages for that date
userMaxDayDictionary = {}  # key=phone, value=[most active date, msgs on that date]
userDaysActiveDictionary = {}  # key=phone, value=[Is Active(0/1), Days active in group]
fullAliasDictionary = {}  # key=phone, value=Name/Alias
emojiCounterDictionary = {}  # key=phone, value=Emoji Counter

prefixes = []
postfixes = []
wordsToIgnore = []

englishMonths = [['Jan', 'Jan', '1'], ['Feb', 'Feb', '2'], ['Mar', 'Mar', '3'], ['Apr', 'Apr', '4'],
                 ['May', 'May', '5'], ['Jun', 'Jun', '6'], ['Jul', 'Jul', '7'], ['Aug', 'Aug', '8'],
                 ['Sep', 'Sep', '9'], ['Oct', 'Oct', '10'], ['Nov', 'Nov', '11'], ['Dec', 'Dec', '12']]
hebrewMonths = [['\u05D1\u05D9\u05E0\u05D5\u05F3', 'Jan', '01'], ['\u05D1\u05E4\u05D1\u05E8\u05F3', 'Feb', '02'],
                ['\u05D1\u05de\u05e8\u05e5', 'Mar', '03'], ['\u05D1\u05d0\u05e4\u05E8\u05F3', 'Apr', '04'],
                ['\u05D1\u05de\u05d0\u05d9', 'May', '05'], ['\u05D1\u05d9\u05D5\u05e0\u05F3', 'Jun', '06'],
                ['\u05D1\u05d9\u05D5\u05dc\u05F3', 'Jul', '07'], ['\u05D1\u05d0\u05D5\u05d2\u05F3', 'Aug', '08'],
                ['\u05D1\u05e1\u05e4\u05d8\u05F3', 'Sep', '09'], ['\u05D1\u05d0\u05D5\u05e7\u05F3', 'Oct', '10'],
                ['\u05D1\u05e0\u05D5\u05d1\u05F3', 'Nov', '11'], ['\u05D1\u05d3\u05e6\u05de\u05F3', 'Dec', '12']]
monthLengths = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
days = ['Invalid', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
totalDayCount = 0
firstDateInFile = ''
lastDateInFile = ''


def main():
    print("Python script for parsing a Whatsapp Emailed Conversation")
    print("Written by Or Barak - contact at efor001@gmail.com")
    print("Version: 01.25")
    print("Last Update: 21.04.2016")
    iNumOfArgs = len(sys.argv)
    if iNumOfArgs != 3 and iNumOfArgs != 4:
        print("Error: Wrong number of arguments:")
        print("usage: ParseEmailConversation.py input_file output_file [Configuration File]")
    else:
        strInputFile = sys.argv[1]
        strOutputFile = sys.argv[2]
        try:
            confFile = sys.argv[3]
        except IndexError:
            confFile = ''
        isHebrew, wOI = Init(confFile)
        ParseFile(strInputFile, strOutputFile, isHebrew, wOI)


def Init(inputConfig):
    try:
        for i in range(1, 8):
            dailyCounterDictionary[days[i]] = 0
        InitAliases(inputConfig)
        isHebrew = InitHebrew(inputConfig)
        wOI = InitWOI(inputConfig)
        global prefixes
        prefixes = InitPrefix(inputConfig)
        global postfixes
        postfixes = InitPostfix(inputConfig)
        global wordsToIgnore
        wordsToIgnore = InitWordsToIgnore(inputConfig)
        return isHebrew, wOI
    except FileNotFoundError:
        print(inputConfig + ' - No File Found! Continuing without Configuration File...')
        return 0, []


def InitAliases(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[Aliases\]'
    endTagString = '\[/Aliases\]'
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                index = re.split('\t', line, 1, flags=re.IGNORECASE)
                try:
                    fullAliasDictionary[index[0]] = index[1][:-1]
                except IndexError:
                    fullAliasDictionary[index[0]] = index[0][:-1]
    if foundTag == None:
        print('Could not find tag [Aliases]!')


def InitHebrew(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[isHebrew\]'
    endTagString = '\[/isHebrew\]'
    retValue = 0
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                matchResult = re.search('yes', line, flags=re.IGNORECASE)
                if matchResult:
                    retValue = 1
    if foundTag == None:
        print('Could not find tag [isHebrew]! Using English as default...')
    return retValue


def InitWOI(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[wordsOfInterest\]'
    endTagString = '\[/wordsOfInterest\]'
    retValue = []
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                wordGroup = re.split(',', line)
                outputWordGroup = []
                for word in wordGroup:
                    if word != '\n':
                        outputWordGroup.append(word)
                retValue.append(outputWordGroup)
    if foundTag == None:
        print('Could not find tag [wordsOfInterest]!')
    return retValue


def InitPrefix(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[Prefixes\]'
    endTagString = '\[/Prefixes\]'
    retValue = []
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                word = re.split('\n', line)
                retValue.append(word[0])
    if foundTag == None:
        print('Could not find tag [Prefixes]!')
    return retValue


def InitPostfix(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[Postfixes\]'
    endTagString = '\[/Postfixes\]'
    retValue = []
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                word = re.split('\n', line)
                retValue.append(word[0])
    if foundTag == None:
        print('Could not find tag [Postfixes]!')
    return retValue


def InitWordsToIgnore(inputConfig):
    foundTag = None
    foundEndTag = None
    startTagString = '\[wordsToIgnore\]'
    endTagString = '\[/wordsToIgnore\]'
    retValue = []
    with open(inputConfig, 'r', encoding="utf-8") as confFile:
        for line in confFile:
            foundEndTag = re.search(endTagString, line)
            if foundEndTag:
                break
            if foundTag == None:
                foundTag = re.search(startTagString, line)
            else:
                word = re.split('\n', line)
                retValue.append(word[0])
    if foundTag == None:
        print('Could not find tag [wordsToIgnore]!')
    return retValue


def ParseFile(strInputFile, strOutputFile, isHebrew, wOI):
    # creare Excel output
    outputWorkBook = Workbook()
    outputWorkSheet = outputWorkBook.active
    outputWorkSheet.title = 'Whatsapp'
    outputWorkSheet.page_setup.fitToPage = True
    outputWorkSheet.page_setup.fitToWidth = 1
    # outputWorkSheet.views.SheetView(rightToLeft)
    outputWorkSheet.cell(row=1, column=1).value = 'Date'
    outputWorkSheet.cell(row=1, column=2).value = 'Year'
    outputWorkSheet.cell(row=1, column=3).value = 'Time'
    outputWorkSheet.cell(row=1, column=4).value = 'Author'
    outputWorkSheet.cell(row=1, column=5).value = 'Message'
    outputWorkSheet.cell(row=1, column=6).value = 'Word Count'
    outputWorkSheet.cell(row=1, column=7).value = 'Is Scoop'
    totalStatisticsWorkSheet = outputWorkBook.create_sheet()
    totalStatisticsWorkSheet.title = 'Total Statistics'
    dayStatisticsWorkSheet = outputWorkBook.create_sheet()
    dayStatisticsWorkSheet.title = 'Last Day\'s Statistics'
    userStatsWorkSheet = outputWorkBook.create_sheet()
    userStatsWorkSheet.title = 'User Statistics'
    userStats2WorkSheet = outputWorkBook.create_sheet()
    userStats2WorkSheet.title = 'User Message Statistics'
    scoopsWorkSheet = outputWorkBook.create_sheet()
    scoopsWorkSheet.title = 'Scoops'

    dateStrOld = ''
    global totalDayCount
    global firstDateInFile
    global lastDateInFile

    out_row_index = 2
    scoopCounter = 0

    lineCounter = 0
    with open(strInputFile, 'r', encoding="utf-8-sig") as inFile:
        for line in inFile:
            lineCounter += 1
    print('Found', lineCounter, 'lines in input file')

    tempLineCounter = 0
    percentageCounter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(0, 9):
        percentageCounter[i] = int(lineCounter / 10) * (i + 1)
    percentageCounter[i + 1] = lineCounter
    progressCounter = 0
    print('Processing Lines', end='', flush=True)
    with open(strInputFile, 'r', encoding="utf-8-sig") as inFile:
        for line in inFile:
            if tempLineCounter==12897:
                strCol5 = ""
            #			print(str(tempLineCounter))
            strCol1 = ''
            strCol2 = ''
            strCol3 = ''
            strCol4 = ''
            strCol5 = ''
            isMessage = None
            isMessageCont = 0
            if len(line) > 0:
                # Messages start in lines with the following format: <date>, <time>: <user-id/number>: <msg>
                # <msg> can be several lines long.
                #
                # TODO: Change this into a simple regular expression.
                index = re.split(', ', line, 1, re.IGNORECASE)
                try:
                    if len(index)!=2:
                        # Some message lines also start with <date>, confusing the parsing.
                        raise IndexError("Did not find second part of line, this is a continuation")
                    retValue = 0
                    isBeginDate = 0
                    index3 = re.split('/', index[0], 2, re.IGNORECASE)
                    if isHebrew == 0:
                        dayToDisplay = index3[1]
                        monthsToCheck = englishMonths
                        monthToCheck = index3[0]
                    else:
                        dayToDisplay = index3[1]
                        monthsToCheck = hebrewMonths
                        monthToCheck = index3[0]
                    for month, englishMonth, monthNumber in monthsToCheck:
                        if monthToCheck == monthNumber:
                            isBeginDate = 1
                            monthToDisplay = englishMonth
                    if isBeginDate == 1:
                        strCol1 = monthToDisplay + ' ' + str(int(dayToDisplay))
                        strCol2 = '20' + index3[2]
                    else:
                        raise IndexError
                except IndexError:
                    if out_row_index > 2:
                        out_row_index -= 1
                        strCol5 = line
                        isMessageCont = 1
                        retValue = -1

                if retValue != -1:
                    index = re.split(': ', index[1], 1, re.IGNORECASE)
                    index4 = re.split(':', index[0], 2, re.IGNORECASE)
                    msgHour = int(index4[0])
                    msgMinute = index4[1]
                    isPM = re.search('PM', index4[2], re.IGNORECASE)
                    if isPM:
                        if msgHour != 12:
                            msgHour += 12
                    else:
                        if msgHour == 12:
                            msgHour -= 12
                    msgHourStr = str(msgHour)
                    if len(msgHourStr) == 1:
                        msgHourStr = '0' + msgHourStr
                    timestampStr = msgHourStr + ':' + msgMinute

                    strCol3 = timestampStr
                    isMessage = re.search(': ', index[1], re.IGNORECASE)
                    if isHebrew == 0:
                        isSubjectChange = re.search('changed the subject to', index[1], re.IGNORECASE)
                        isAdd = re.search('added', index[1], re.IGNORECASE)
                        isRemove = re.search('removed', index[1], re.IGNORECASE)
                        isLeft = re.search('left', index[1], re.IGNORECASE)
                    else:
                        isSubjectChange = re.search('changed the subject to', index[1], re.IGNORECASE)
                        isAdd = re.search('added', index[1], re.IGNORECASE)
                        isRemove = re.search('removed', index[1], re.IGNORECASE)
                        isLeft = re.search('left', index[1], re.IGNORECASE)

                    if isMessage and not isSubjectChange:
                        index = re.split(': ', index[1], 1, re.IGNORECASE)
                        strCol4, emojiCount = RemoveEmojis(index[0])
                        if strCol4 == 'Or Barak':
                            strCol4 = '+972 54-999-5488'
                        try:
                            strCol4 = fullAliasDictionary[strCol4]
                        except KeyError:
                            strCol4 = strCol4
                        strCol5 = index[1]
                    elif isMessage == None and isAdd != None:
                        if isHebrew == 0:
                            index = re.split('added ', index[1], 1, re.IGNORECASE)
                        else:
                            index = re.split('added ', index[1], 1, re.IGNORECASE)
                        newMember, temp = RemoveEmojis(index[1][:-1])
                        if newMember in fullAliasDictionary:
                            member = fullAliasDictionary[newMember]
                        else:
                            member = newMember
                        if member not in userDaysActiveDictionary:
                            userDaysActiveDictionary[member] = [1, 0]
                        else:
                            userDaysActiveDictionary[member][0] = 1
                    elif (isMessage == None) and (isLeft != None or isRemove != None):
                        if isHebrew == 0:
                            newIndex = re.split(' left', index[1], 1, re.IGNORECASE)
                            if isLeft != None:
                                newMember, temp = RemoveEmojis(newIndex[0])
                                if newMember in fullAliasDictionary:
                                    member = fullAliasDictionary[newMember]
                                else:
                                    member = newMember
                            else:
                                newIndex = re.split('removed ', index[1], 1, re.IGNORECASE)
                                try:
                                    newMember, temp = RemoveEmojis(newIndex[1][:-1])
                                except IndexError:
                                    newIndex = re.split(' was removed', index[1], 1, re.IGNORECASE)
                                    newMember, temp = RemoveEmojis(newIndex[0])
                                if newMember in fullAliasDictionary:
                                    member = fullAliasDictionary[newMember]
                                else:
                                    member = newMember
                        else:
                            newIndex = re.split(' left', index[1], 1, re.IGNORECASE)
                            if isLeft != None:
                                newMember, temp = RemoveEmojis(newIndex[0])
                                if newMember in fullAliasDictionary:
                                    member = fullAliasDictionary[newMember]
                                else:
                                    member = newMember
                            else:
                                newIndex = re.split('removed ', index[1], 1, re.IGNORECASE)
                                newMember, temp = RemoveEmojis(newIndex[1][:-1])
                                if newMember in fullAliasDictionary:
                                    member = fullAliasDictionary[newMember]
                                else:
                                    member = newMember
                        if member not in userDaysActiveDictionary:
                            userDaysActiveDictionary[member] = [0, totalDayCount]
                        else:
                            userDaysActiveDictionary[member][0] = 0

                isScoop = CheckForScoopEmoji(strCol5)
                strCol5, emojiCount = RemoveEmojis(strCol5)
                try:
                    emojiCounterDictionary[strCol4] += emojiCount
                except KeyError:
                    emojiCounterDictionary[strCol4] = emojiCount
                if (isMessage and not isSubjectChange) or isMessageCont == 1:
                    messageWordCount = int(CountWords(strCol5, isHebrew))
                    if isMessageCont == 1:
                        strCol4 = outputWorkSheet.cell(row=out_row_index, column=4).value
                        numberWordDictionary[strCol4] += messageWordCount
                        todayNumberWordDictionary[strCol4] += messageWordCount
                        outputWorkSheet.cell(row=out_row_index, column=6).value += messageWordCount
                        outputWorkSheet.cell(row=out_row_index, column=5).value = outputWorkSheet.cell(
                            row=out_row_index, column=5).value + strCol5
                        if outputWorkSheet.cell(row=out_row_index, column=7).value == 1 and isScoop == 1:
                            scoopCounter -= 1
                        outputWorkSheet.cell(row=out_row_index, column=7).value = min(1, outputWorkSheet.cell(
                            row=out_row_index, column=7).value + isScoop)
                    else:
                        outputWorkSheet.cell(row=out_row_index, column=1).value = strCol1
                        outputWorkSheet.cell(row=out_row_index, column=2).value = strCol2
                        outputWorkSheet.cell(row=out_row_index, column=3).value = strCol3
                        outputWorkSheet.cell(row=out_row_index, column=4).value = strCol4
                        if strCol5 and strCol5[0] == '=':
                            strCol5 = '\'' + strCol5
                        outputWorkSheet.cell(row=out_row_index, column=5).value = strCol5
                        outputWorkSheet.cell(row=out_row_index, column=6).value = messageWordCount
                        outputWorkSheet.cell(row=out_row_index, column=7).value = isScoop

                        # add to message statistics for the message date
                        # parse dateDictionary key
                        dateStr = ParseDate(strCol1, strCol2)

                        # if date changed, reset word counters for last day
                        if dateStrOld != dateStr:
                            if dateStrOld == '':
                                firstDateInFile = dateStr
                            else:

                                totalDayCount += 1

                                # add to user Max Day Stat
                                for key in todayNumberWordDictionary.keys():
                                    if key not in userMaxDayDictionary:
                                        userMaxDayDictionary[key] = [dateStrOld, todayNumberMsgDictionary[key]]
                                    else:
                                        currentMax = userMaxDayDictionary[key][1]
                                        if currentMax < todayNumberMsgDictionary[key]:
                                            userMaxDayDictionary[key] = [dateStrOld, todayNumberMsgDictionary[key]]
                                    if key not in userMessageCounterDictionary:
                                        userMessageCounterDictionary[key] = {}
                                        userMessageCounterDictionary[key][dateStrOld] = todayNumberMsgDictionary[key]
                                    else:
                                        userMessageCounterDictionary[key][dateStrOld] = todayNumberMsgDictionary[key]

                                for key in userDaysActiveDictionary.keys():
                                    if userDaysActiveDictionary[key][0] == 1:
                                        daysActive = userDaysActiveDictionary[key][1]
                                        userDaysActiveDictionary[key][1] = daysActive + 1

                            dayOfTheWeek = CalculateDayOfTheWeek(dateStr)
                            for key in todayDictionary.keys():
                                todayDictionary[key] = 0
                            for key in todayNumberMsgDictionary.keys():
                                todayNumberMsgDictionary[key] = 0
                            for key in todayNumberWordDictionary.keys():
                                todayNumberWordDictionary[key] = 0
                            for key in hourDictionary.keys():
                                hourDictionary[key] = 0

                        dateStrOld = dateStr

                        try:
                            dateDictionary[dateStr] += 1
                        except KeyError:
                            dateDictionary[dateStr] = 1
                        try:
                            hourDictionary[strCol3[0:2]] += 1
                        except KeyError:
                            hourDictionary[strCol3[0:2]] = 1
                        try:
                            hourAccDictionary[strCol3[0:2]] += 1
                        except KeyError:
                            hourAccDictionary[strCol3[0:2]] = 1

                        # add to message statistics for the number/contact
                        try:
                            numberMsgDictionary[strCol4] += 1
                        except KeyError:
                            numberMsgDictionary[strCol4] = 1
                        try:
                            todayNumberMsgDictionary[strCol4] += 1
                        except KeyError:
                            todayNumberMsgDictionary[strCol4] = 1

                        # add to message statistics for the number/contact
                        try:
                            numberWordDictionary[strCol4] += messageWordCount
                        except KeyError:
                            numberWordDictionary[strCol4] = messageWordCount
                        # add to message statistics for the number/contact
                        try:
                            todayNumberWordDictionary[strCol4] += messageWordCount
                        except KeyError:
                            todayNumberWordDictionary[strCol4] = messageWordCount

                        # add to user Daily Counter
                        if strCol4 not in userDailyCounterDictionary:
                            userDailyCounterDictionary[strCol4] = {}
                        try:
                            userDailyCounterDictionary[strCol4][days[dayOfTheWeek]] += 1
                        except KeyError:
                            userDailyCounterDictionary[strCol4][days[dayOfTheWeek]] = 1
                        dailyCounterDictionary[days[dayOfTheWeek]] += 1

                        # add to user Hourly Counter
                        hourKey = str(strCol3[0:2]) + ':00-' + str(strCol3[0:2]) + ':59'
                        if strCol4 not in userHourCounterDictionary:
                            userHourCounterDictionary[strCol4] = {}
                        try:
                            userHourCounterDictionary[strCol4][hourKey] += 1
                        except KeyError:
                            userHourCounterDictionary[strCol4][hourKey] = 1

                    if isScoop:
                        # Copy to Scoops Sheet
                        scoopsWorkSheet.cell(row=scoopCounter + 1, column=1).value = outputWorkSheet.cell(
                            row=out_row_index, column=1).value
                        scoopsWorkSheet.cell(row=scoopCounter + 1, column=2).value = outputWorkSheet.cell(
                            row=out_row_index, column=2).value
                        scoopsWorkSheet.cell(row=scoopCounter + 1, column=3).value = outputWorkSheet.cell(
                            row=out_row_index, column=3).value
                        scoopsWorkSheet.cell(row=scoopCounter + 1, column=4).value = outputWorkSheet.cell(
                            row=out_row_index, column=4).value
                        scoopsWorkSheet.cell(row=scoopCounter + 1, column=5).value = outputWorkSheet.cell(
                            row=out_row_index, column=5).value
                        scoopCounter += 1
                    out_row_index += 1

            tempLineCounter += 1
            if progressCounter < 10:
                if tempLineCounter >= percentageCounter[progressCounter]:
                    progressCounter += 1
                    print('.', end='', flush=True)
            else:
                print('Unexpected Line!')

        totalDayCount += 1

        # add to user Max Day Stat
        for key in todayNumberWordDictionary.keys():
            if key not in userMaxDayDictionary:
                userMaxDayDictionary[key] = [dateStr, todayNumberMsgDictionary[key]]
            else:
                currentMax = userMaxDayDictionary[key][1]
                if currentMax < todayNumberMsgDictionary[key]:
                    userMaxDayDictionary[key] = [dateStr, todayNumberMsgDictionary[key]]
            if key not in userMessageCounterDictionary:
                userMessageCounterDictionary[key] = {}
                userMessageCounterDictionary[key][dateStr] = todayNumberMsgDictionary[key]
            else:
                userMessageCounterDictionary[key][dateStr] = todayNumberMsgDictionary[key]

        for key in userDaysActiveDictionary.keys():
            if userDaysActiveDictionary[key][0] == 1:
                daysActive = userDaysActiveDictionary[key][1]
                userDaysActiveDictionary[key][1] = daysActive + 1

        lastDateInFile = dateStr
        numberList = numberMsgDictionary.items()
        print('Done!')
        print('Writing Overall Statistics...', end='', flush=True)
        WriteOverallStatistics(totalStatisticsWorkSheet, wOI)
        print('Done!')
        print('Writing Last Day Statistics...', end='', flush=True)
        WriteLastDayStatistics(dayStatisticsWorkSheet, wOI)
        print('Done!')
        print('Writing User Statistics...', end='', flush=True)
        WriteUserStatistics(userStatsWorkSheet, dateStr)
        WriteUserStatistics2(userStats2WorkSheet)
        print('Done!')
        #		print('Writing Scoops Statistics...',end='',flush=True)
        #		WriteTodaysScoops(scoopsWorkSheet,dateStrOld,scoopCounter)
        #		print('Done!')

        strFinalFileName = strOutputFile + ".xlsx"

        written = 0
        while written == 0:
            try:
                print("Writing to", strFinalFileName)
                outputWorkBook.save(strFinalFileName)
                written = 1
            except PermissionError:
                newFileName = input("Permission Denied! Enter new filename or Press Enter to retry:")
                if len(newFileName) != 0:
                    strFinalFileName = newFileName + ".xlsx"


def WriteOverallStatistics(outputWorkSheet, wordOfInterest):
    out_row_index = 1
    minimumMsgsForWordsPerMsg = 250

    # Write Msg-per-Date Stats
    daysToDisplay = 15
    totalMsgCount = 0
    global totalDayCount
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Date'
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Day'
    outputWorkSheet.cell(row=out_row_index, column=3).value = 'Messages'
    out_row_index += 1
    maxMsgs = 0
    maxDay = ''
    minMsgs = 10000
    minDay = ''
    for currentDate in dateDictionary:
        totalMsgCount += dateDictionary[currentDate]
        # totalDayCount+=1
        if currentDate == '2015-01-05':
            continue
        if dateDictionary[currentDate] > maxMsgs:
            maxMsgs = dateDictionary[currentDate]
            maxDay = currentDate
        if dateDictionary[currentDate] < minMsgs:
            minMsgs = dateDictionary[currentDate]
            minDay = currentDate
    daysToIgnore = max(0, totalDayCount - daysToDisplay)
    for currentDate in sorted(dateDictionary):
        if (daysToIgnore > 0):
            daysToIgnore -= 1
        else:
            outputWorkSheet.cell(row=out_row_index, column=1).value = currentDate
            dayOfTheWeek = CalculateDayOfTheWeek(currentDate)
            outputWorkSheet.cell(row=out_row_index, column=2).value = days[dayOfTheWeek]
            outputWorkSheet.cell(row=out_row_index, column=3).value = dateDictionary[currentDate]
            out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Average'
    outputWorkSheet.cell(row=out_row_index, column=3).value = totalMsgCount / totalDayCount
    outputWorkSheet.cell(row=out_row_index, column=3).number_format = '0.00'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Max'
    outputWorkSheet.cell(row=out_row_index, column=2).value = maxDay
    outputWorkSheet.cell(row=out_row_index, column=3).value = maxMsgs
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Min'
    outputWorkSheet.cell(row=out_row_index, column=2).value = minDay
    outputWorkSheet.cell(row=out_row_index, column=3).value = minMsgs

    out_row_index += 2

    # Write WoI Stats
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=1, end_column=2)
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Words Of Interest'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Total'
    out_row_index += 1
    for wordGroup in wordOfInterest:
        outputWorkSheet.cell(row=out_row_index, column=1).value = ''
        outputWorkSheet.cell(row=out_row_index, column=2).value = 0
        for word in wordGroup:
            outputWorkSheet.cell(row=out_row_index, column=1).value += word + '\\'
            try:
                outputWorkSheet.cell(row=out_row_index, column=2).value += dictionary[word] + CheckPrefixes(word,
                                                                                                            dictionary) + CheckPostfixes(
                    word, dictionary)
            except KeyError:
                outputWorkSheet.cell(row=out_row_index, column=2).value += CheckPrefixes(word,
                                                                                         dictionary) + CheckPostfixes(
                    word, dictionary)
        outputWorkSheet.cell(row=out_row_index, column=1).value = outputWorkSheet.cell(row=out_row_index,
                                                                                       column=1).value[0:len(
            outputWorkSheet.cell(row=out_row_index, column=1).value) - 1]
        out_row_index += 1

    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Hour'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Messages'
    out_row_index += 1
    for currentHour in sorted(hourAccDictionary):
        outputWorkSheet.cell(row=out_row_index, column=1).value = currentHour + ':00-' + currentHour + ':59'
        try:
            outputWorkSheet.cell(row=out_row_index, column=2).value = hourAccDictionary[currentHour]
        except KeyError:
            hourAccDictionary[currentHour] = 0
            outputWorkSheet.cell(row=out_row_index, column=2).value = hourAccDictionary[currentHour]
        out_row_index += 1

    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Day of Week'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Messages'
    out_row_index += 1
    for currentDay in sorted(dailyCounterDictionary):
        outputWorkSheet.cell(row=out_row_index, column=1).value = currentDay
        try:
            outputWorkSheet.cell(row=out_row_index, column=2).value = dailyCounterDictionary[currentDay]
        except KeyError:
            dailyCounterDictionary[currentDay] = 0
            outputWorkSheet.cell(row=out_row_index, column=2).value = dailyCounterDictionary[currentDay]
        out_row_index += 1

    # Write number msg/word stats
    out_row_index = 1
    outputWorkSheet.cell(row=out_row_index, column=4).value = 'Word Rank'
    outputWorkSheet.cell(row=out_row_index, column=5).value = 'Msg Rank'
    outputWorkSheet.cell(row=out_row_index, column=6).value = 'User'
    outputWorkSheet.cell(row=out_row_index, column=7).value = 'Messages'
    outputWorkSheet.cell(row=out_row_index, column=8).value = 'Words'
    outputWorkSheet.cell(row=out_row_index, column=9).value = ' '
    outputWorkSheet.column_dimensions['I'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=10).value = 'Rank'
    outputWorkSheet.column_dimensions['J'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=11).value = 'User'
    outputWorkSheet.column_dimensions['K'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=12).value = 'Messages'
    outputWorkSheet.column_dimensions['L'].width = 9
    outputWorkSheet.cell(row=out_row_index, column=13).value = 'Words/Message'
    outputWorkSheet.column_dimensions['M'].width = 15
    outputWorkSheet.cell(row=out_row_index, column=14).value = ' '
    outputWorkSheet.column_dimensions['N'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=15).value = 'User'
    outputWorkSheet.column_dimensions['O'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=16).value = 'Words'
    outputWorkSheet.column_dimensions['P'].width = 7
    outputWorkSheet.cell(row=out_row_index, column=17).value = ' '
    outputWorkSheet.column_dimensions['Q'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=18).value = 'User'
    outputWorkSheet.column_dimensions['R'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=19).value = 'Words/Message'
    outputWorkSheet.column_dimensions['S'].width = 15
    out_row_index += 1
    for currentNumber in numberMsgDictionary.keys():
        outputWorkSheet.cell(row=out_row_index, column=4).value = '=RANK($H' + str(out_row_index) + ',$H$2:$H$' + str(
            len(numberMsgDictionary) + 1) + ',0)'
        outputWorkSheet.cell(row=out_row_index, column=5).value = '=RANK($G' + str(out_row_index) + ',$G$2:$G$' + str(
            len(numberMsgDictionary) + 1) + ',0)'
        outputWorkSheet.cell(row=out_row_index, column=6).value = currentNumber
        outputWorkSheet.cell(row=out_row_index, column=7).value = numberMsgDictionary[
                                                                      currentNumber] + out_row_index * 0.00001
        outputWorkSheet.cell(row=out_row_index, column=8).value = numberWordDictionary[
                                                                      currentNumber] + out_row_index * 0.00001
        outputWorkSheet.cell(row=out_row_index, column=10).value = out_row_index - 1
        outputWorkSheet.cell(row=out_row_index, column=11).value = '=IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(numberMsgDictionary) + 1) + ',2,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(numberMsgDictionary) + 1) + ',2,TRUE))'
        outputWorkSheet.cell(row=out_row_index, column=12).value = '=TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(numberMsgDictionary) + 1) + ',3,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(numberMsgDictionary) + 1) + ',3,TRUE)))'
        outputWorkSheet.cell(row=out_row_index, column=13).value = '=IF($L' + str(out_row_index) + '>=' + str(
            minimumMsgsForWordsPerMsg) + ',(VLOOKUP($K' + str(out_row_index) + ',$O$1:$P' + str(
            len(numberMsgDictionary) + 1) + ',2,FALSE)/$L' + str(out_row_index) + ')+' + str(
            out_row_index * 0.00001) + ',' + str(out_row_index * 0.00001) + ')'
        outputWorkSheet.cell(row=out_row_index, column=15).value = '=IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(numberMsgDictionary) + 1) + ',3,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(numberMsgDictionary) + 1) + ',3,TRUE))'
        outputWorkSheet.cell(row=out_row_index, column=16).value = '=TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(numberMsgDictionary) + 1) + ',5,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(numberMsgDictionary) + 1) + ',5,TRUE)))'
        outputWorkSheet.cell(row=out_row_index, column=18).value = '=INDEX($K$2:$M$' + str(
            len(numberMsgDictionary) + 1) + ',MATCH(LARGE($M$2:$M$' + str(len(numberMsgDictionary) + 1) + ',' + str(
            out_row_index - 1) + '),$M$2:$M$' + str(len(numberMsgDictionary) + 1) + ',0),1)'
        outputWorkSheet.cell(row=out_row_index, column=19).value = '=LARGE($M$2:$M$' + str(
            len(numberMsgDictionary) + 1) + ',' + str(out_row_index - 1) + ')'
        outputWorkSheet.cell(row=out_row_index, column=19).number_format = '0.00'
        out_row_index += 1
    outputWorkSheet.column_dimensions.group('D', 'H', hidden=True)
    outputWorkSheet.column_dimensions.group('M', 'M', hidden=True)

    # Write top words
    maxTopWords = 100
    out_row_index = 1
    dictionaryList = dictionary.items()
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=21, end_column=22)
    outputWorkSheet.cell(row=out_row_index, column=21).value = 'Top Used Words'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=21).value = 'Word'
    outputWorkSheet.cell(row=out_row_index, column=22).value = 'Times Used'
    out_row_index += 1
    counter = 1
    for item in sorted(dictionaryList, key=sort_key, reverse=True):
        if IgnoreWord(item[0]) != 1:
            strToDisplay = item[0]
            if strToDisplay[0] == '=':
                strToDisplay = '\'' + strToDisplay
            outputWorkSheet.cell(row=out_row_index, column=21).value = strToDisplay
            outputWorkSheet.cell(row=out_row_index, column=22).value = (item[1] + CheckPrefixes(item[0],
                                                                                                dictionary) + CheckPostfixes(
                item[0], dictionary)) + counter / (maxTopWords * maxTopWords)
            counter += 1
            if counter > maxTopWords:
                break
            out_row_index += 1
    outputWorkSheet.column_dimensions.group('U', 'V', hidden=True)
    out_row_index = 1
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=23, end_column=24)
    outputWorkSheet.cell(row=out_row_index, column=23).value = 'Top Used Words'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=23).value = 'Word'
    outputWorkSheet.cell(row=out_row_index, column=24).value = 'Times Used'
    out_row_index += 1
    for i in range(1, maxTopWords + 1):
        outputWorkSheet.cell(row=out_row_index, column=23).value = '=INDEX($U$3:$V$' + str(
            maxTopWords + 2) + ',MATCH(LARGE($V$3:$V$' + str(maxTopWords + 2) + ',' + str(i) + '),$V$3:$V$' + str(
            maxTopWords + 2) + ',0),1)'
        outputWorkSheet.cell(row=out_row_index, column=24).value = '=TRUNC(LARGE($V$3:$V$' + str(
            maxTopWords + 2) + ',' + str(i) + '))'
        out_row_index += 1


def WriteLastDayStatistics(outputWorkSheet, wordOfInterest):
    out_row_index = 1
    minimumMsgsForWordsPerMsg = 10

    # Write Hourly Stats
    totalMessages = 0
    totalHourInADay = 24
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=1, end_column=2)
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Messages Per Hour'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Hour'
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Messages'
    out_row_index += 1
    for currentHour in sorted(hourDictionary):
        outputWorkSheet.cell(row=out_row_index, column=1).value = currentHour + ':00-' + currentHour + ':59'
        try:
            outputWorkSheet.cell(row=out_row_index, column=2).value = hourDictionary[currentHour]
        except KeyError:
            hourDictionary[currentHour] = 0
            outputWorkSheet.cell(row=out_row_index, column=2).value = hourDictionary[currentHour]
        out_row_index += 1
        totalMessages += hourDictionary[currentHour]
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Average'
    outputWorkSheet.cell(row=out_row_index, column=2).value = totalMessages / totalHourInADay
    outputWorkSheet.cell(row=out_row_index, column=2).number_format = '0.00'
    out_row_index += 2

    # Write WoI Stats
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=1, end_column=2)
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Words Of Interest'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Last Day'
    out_row_index += 1
    for wordGroup in wordOfInterest:
        outputWorkSheet.cell(row=out_row_index, column=1).value = ''
        outputWorkSheet.cell(row=out_row_index, column=2).value = 0
        for word in wordGroup:
            outputWorkSheet.cell(row=out_row_index, column=1).value += word + '\\'
            try:
                outputWorkSheet.cell(row=out_row_index, column=2).value += todayDictionary[word] + CheckPrefixes(word,
                                                                                                                 todayDictionary) + CheckPostfixes(
                    word, todayDictionary)
            except KeyError:
                outputWorkSheet.cell(row=out_row_index, column=2).value += CheckPrefixes(word,
                                                                                         todayDictionary) + CheckPostfixes(
                    word, todayDictionary)
        outputWorkSheet.cell(row=out_row_index, column=1).value = outputWorkSheet.cell(row=out_row_index,
                                                                                       column=1).value[0:len(
            outputWorkSheet.cell(row=out_row_index, column=1).value) - 1]
        out_row_index += 1

    # Write number msg/word stats
    out_row_index = 1
    outputWorkSheet.cell(row=out_row_index, column=4).value = 'Word Rank'
    outputWorkSheet.cell(row=out_row_index, column=5).value = 'Msg Rank'
    outputWorkSheet.cell(row=out_row_index, column=6).value = 'User'
    outputWorkSheet.cell(row=out_row_index, column=7).value = 'Messages'
    outputWorkSheet.cell(row=out_row_index, column=8).value = 'Words'
    outputWorkSheet.cell(row=out_row_index, column=9).value = ' '
    outputWorkSheet.column_dimensions['I'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=10).value = 'Rank'
    outputWorkSheet.column_dimensions['J'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=11).value = 'User'
    outputWorkSheet.column_dimensions['K'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=12).value = 'Messages'
    outputWorkSheet.column_dimensions['L'].width = 9
    outputWorkSheet.cell(row=out_row_index, column=13).value = 'Words/Message'
    outputWorkSheet.column_dimensions['M'].width = 15
    outputWorkSheet.cell(row=out_row_index, column=14).value = ' '
    outputWorkSheet.column_dimensions['N'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=15).value = 'User'
    outputWorkSheet.column_dimensions['O'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=16).value = 'Words'
    outputWorkSheet.column_dimensions['P'].width = 7
    outputWorkSheet.cell(row=out_row_index, column=17).value = ' '
    outputWorkSheet.column_dimensions['Q'].width = 5
    outputWorkSheet.cell(row=out_row_index, column=18).value = 'User'
    outputWorkSheet.column_dimensions['R'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=19).value = 'Words/Message'
    outputWorkSheet.column_dimensions['S'].width = 15
    out_row_index += 1
    for currentNumber in todayNumberMsgDictionary.keys():
        outputWorkSheet.cell(row=out_row_index, column=4).value = '=RANK($H' + str(out_row_index) + ',$H$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',0)'
        outputWorkSheet.cell(row=out_row_index, column=5).value = '=RANK($G' + str(out_row_index) + ',$G$2:$G$' + str(
            len(todayNumberMsgDictionary) + 1) + ',0)'
        outputWorkSheet.cell(row=out_row_index, column=6).value = currentNumber
        outputWorkSheet.cell(row=out_row_index, column=7).value = todayNumberMsgDictionary[
                                                                      currentNumber] + out_row_index * 0.00001
        outputWorkSheet.cell(row=out_row_index, column=8).value = todayNumberWordDictionary[
                                                                      currentNumber] + out_row_index * 0.00001
        outputWorkSheet.cell(row=out_row_index, column=10).value = out_row_index - 1
        outputWorkSheet.cell(row=out_row_index, column=11).value = '=IF($L' + str(
            out_row_index) + '<>"",IFERROR(VLOOKUP($J' + str(out_row_index) + ',$E$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',2,FALSE), VLOOKUP($J' + str(out_row_index) + ',$E$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',2,TRUE)),"")'
        outputWorkSheet.cell(row=out_row_index, column=12).value = '=IF(TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',3,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',3,TRUE)))>=1,TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',3,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$E$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',3,TRUE))),"")'
        outputWorkSheet.cell(row=out_row_index, column=13).value = '=IF(AND($L' + str(out_row_index) + '<>"",$L' + str(
            out_row_index) + '>=' + str(minimumMsgsForWordsPerMsg) + '),(VLOOKUP($K' + str(
            out_row_index) + ',$O$1:$P' + str(len(todayNumberMsgDictionary) + 1) + ',2,FALSE)/$L' + str(
            out_row_index) + ')+' + str(out_row_index * 0.00001) + ',' + str(out_row_index * 0.00001) + ')'
        outputWorkSheet.cell(row=out_row_index, column=15).value = '=IF($P' + str(
            out_row_index) + '<>"",IFERROR(VLOOKUP($J' + str(out_row_index) + ',$D$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',3,FALSE), VLOOKUP($J' + str(out_row_index) + ',$D$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',3,TRUE)),"")'
        outputWorkSheet.cell(row=out_row_index, column=16).value = '=IF(TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',5,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(
            len(todayNumberMsgDictionary) + 1) + ',5,TRUE)))>=1,TRUNC(IFERROR(VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',5,FALSE), VLOOKUP($J' + str(
            out_row_index) + ',$D$2:$H$' + str(len(todayNumberMsgDictionary) + 1) + ',5,TRUE))),"")'
        outputWorkSheet.cell(row=out_row_index, column=18).value = '=INDEX($K$2:$M$' + str(
            len(todayNumberMsgDictionary) + 1) + ',MATCH(LARGE($M$2:$M$' + str(
            len(todayNumberMsgDictionary) + 1) + ',' + str(out_row_index - 1) + '),$M$2:$M$' + str(
            len(todayNumberMsgDictionary) + 1) + ',0),1)'
        outputWorkSheet.cell(row=out_row_index, column=19).value = '=IF($R' + str(
            out_row_index) + '<>"",LARGE($M$2:$M$' + str(len(todayNumberMsgDictionary) + 1) + ',' + str(
            out_row_index - 1) + '),"")'
        outputWorkSheet.cell(row=out_row_index, column=19).number_format = '0.00'
        out_row_index += 1
    outputWorkSheet.column_dimensions.group('D', 'H', hidden=True)
    outputWorkSheet.column_dimensions.group('M', 'M', hidden=True)

    # Write top words
    maxTopWords = 100
    out_row_index = 1
    dictionaryList = todayDictionary.items()
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=21, end_column=22)
    outputWorkSheet.cell(row=out_row_index, column=21).value = 'Top Used Words'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=21).value = 'Word'
    outputWorkSheet.cell(row=out_row_index, column=22).value = 'Times Used'
    out_row_index += 1
    counter = 1
    for item in sorted(dictionaryList, key=sort_key_today, reverse=True):
        if IgnoreWord(item[0]) != 1:
            strToDisplay = item[0]
            if strToDisplay[0] == '=':
                strToDisplay = '\'' + strToDisplay
            outputWorkSheet.cell(row=out_row_index, column=21).value = strToDisplay
            outputWorkSheet.cell(row=out_row_index, column=22).value = (item[1] + CheckPrefixes(item[0],
                                                                                                todayDictionary) + CheckPostfixes(
                item[0], todayDictionary)) + counter / (maxTopWords * maxTopWords)
            counter += 1
            if counter > maxTopWords:
                break
            out_row_index += 1
    outputWorkSheet.column_dimensions.group('U', 'V', hidden=True)
    out_row_index = 1
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=23, end_column=24)
    outputWorkSheet.cell(row=out_row_index, column=23).value = 'Top Used Words'
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=23).value = 'Word'
    outputWorkSheet.cell(row=out_row_index, column=24).value = 'Times Used'
    out_row_index += 1
    for i in range(1, maxTopWords + 1):
        outputWorkSheet.cell(row=out_row_index, column=23).value = '=INDEX($U$3:$V$' + str(
            maxTopWords + 2) + ',MATCH(LARGE($V$3:$V$' + str(maxTopWords + 2) + ',' + str(i) + '),$V$3:$V$' + str(
            maxTopWords + 2) + ',0),1)'
        outputWorkSheet.cell(row=out_row_index, column=24).value = '=TRUNC(LARGE($V$3:$V$' + str(
            maxTopWords + 2) + ',' + str(i) + '))'
        out_row_index += 1


def WriteUserStatistics(outputWorkSheet, dateStr):
    out_row_index = 1
    maxUsers = 200
    numberList = numberMsgDictionary.items()

    outputWorkSheet.cell(row=out_row_index, column=1).value = 'User'
    outputWorkSheet.column_dimensions['A'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Messages/Day'
    outputWorkSheet.column_dimensions['B'].width = 14
    outputWorkSheet.cell(row=out_row_index, column=3).value = 'Words/Day'
    outputWorkSheet.cell(row=out_row_index, column=4).value = 'Most Active DoW'
    outputWorkSheet.column_dimensions['D'].width = 15
    outputWorkSheet.cell(row=out_row_index, column=5).value = 'Most Active HoD'
    outputWorkSheet.column_dimensions['E'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=6).value = 'Emoji Counter'
    outputWorkSheet.column_dimensions['F'].width = 15
    outputWorkSheet.cell(row=out_row_index, column=7).value = 'Most Active Day'
    outputWorkSheet.column_dimensions['G'].width = 15
    outputWorkSheet.cell(row=out_row_index, column=8).value = 'Messages'
    outputWorkSheet.cell(row=out_row_index, column=9).value = 'Day Active'
    outputWorkSheet.column_dimensions['I'].width = 14
    out_row_index += 1

    for currentNumber in sorted(numberList, key=sort_key_msg, reverse=True):
        try:
            outputWorkSheet.cell(row=out_row_index, column=9).value = userDaysActiveDictionary[currentNumber[0]][1]
        #			if outputWorkSheet.cell(row = out_row_index,column = 9).value == totalDayCount:
        #				outputWorkSheet.cell(row = out_row_index,column = 9).style.font = Font(bold=True)
        except KeyError:
            outputWorkSheet.cell(row=out_row_index, column=9).value = totalDayCount
        #			outputWorkSheet.cell(row = out_row_index,column = 9).style.font = Font(bold=True)
        outputWorkSheet.cell(row=out_row_index, column=1).value = currentNumber[0]
        if outputWorkSheet.cell(row=out_row_index, column=9).value == 0:
            outputWorkSheet.cell(row=out_row_index, column=2).value = 0
            outputWorkSheet.cell(row=out_row_index, column=3).value = 0
        else:
            outputWorkSheet.cell(row=out_row_index, column=2).value = currentNumber[1] / outputWorkSheet.cell(
                row=out_row_index, column=9).value
            outputWorkSheet.cell(row=out_row_index, column=3).value = numberWordDictionary[
                                                                          currentNumber[0]] / outputWorkSheet.cell(
                row=out_row_index, column=9).value
        outputWorkSheet.cell(row=out_row_index, column=2).number_format = '0.00'
        outputWorkSheet.cell(row=out_row_index, column=3).number_format = '0.00'
        dictionaryList = userDailyCounterDictionary[currentNumber[0]].items()
        for mostActive in sorted(dictionaryList, key=sort_key_active):
            outputWorkSheet.cell(row=out_row_index, column=4).value = mostActive[0]
        dictionaryList = userHourCounterDictionary[currentNumber[0]].items()
        for mostActive in sorted(dictionaryList, key=sort_key_active):
            outputWorkSheet.cell(row=out_row_index, column=5).value = mostActive[0]
        outputWorkSheet.cell(row=out_row_index, column=6).value = emojiCounterDictionary[currentNumber[0]]
        outputWorkSheet.cell(row=out_row_index, column=7).value = userMaxDayDictionary[currentNumber[0]][0]
        outputWorkSheet.cell(row=out_row_index, column=8).value = userMaxDayDictionary[currentNumber[0]][1]
        #		if userMaxDayDictionary[currentNumber[0]][0] == dateStr:
        #			outputWorkSheet.cell(row = out_row_index,column = 7).style.font = Font(bold=True)
        #			outputWorkSheet.cell(row = out_row_index,column = 8).style.font = Font(bold=True)
        out_row_index += 1
        maxUsers -= 1
        if maxUsers == 0:
            break


def WriteUserStatistics2(outputWorkSheet):
    out_row_index = 2
    maxUsers = 20
    numberList = numberMsgDictionary.items()

    temp = maxUsers
    for currentNumber in sorted(numberList, key=sort_key_msg, reverse=True):
        outputWorkSheet.cell(row=out_row_index, column=1).value = currentNumber[0]
        out_row_index += 1
        temp -= 1
        if temp == 0:
            break

    currentDate = firstDateInFile
    colIndex = 2
    while colIndex > 0:
        out_row_index = 1
        outputWorkSheet.cell(row=out_row_index, column=colIndex).value = currentDate
        out_row_index += 1
        for out_row_index in range(2, maxUsers + 2):
            key = outputWorkSheet.cell(row=out_row_index, column=1).value
            try:
                msg = userMessageCounterDictionary[key][currentDate]
            except KeyError:
                msg = 0
            if colIndex == 2:
                outputWorkSheet.cell(row=out_row_index, column=colIndex).value = msg
            else:
                outputWorkSheet.cell(row=out_row_index, column=colIndex).value = outputWorkSheet.cell(row=out_row_index,
                                                                                                      column=colIndex - 1).value + msg
        if currentDate == lastDateInFile:
            colIndex = 0
        else:
            colIndex += 1
        currentDate = IncrementDate(currentDate)


def WriteTodaysScoops(outputWorkSheet, lastDay, scoopCounter):
    # Find number of todays scoops:
    todaysScoops = 0
    for i in range(1, scoopCounter + 1):
        strCol1 = outputWorkSheet.cell(row=i, column=1).value
        strCol2 = outputWorkSheet.cell(row=i, column=2).value
        currentDate = ParseDate(strCol1, strCol2)
        if currentDate == lastDay:
            todaysScoops += 1

    # Move general scoops down to make room for today's scoops:
    for i in range(scoopCounter, 0, -1):
        outputWorkSheet.cell(row=i + todaysScoops + 5, column=1).value = outputWorkSheet.cell(row=i, column=1).value
        outputWorkSheet.cell(row=i + todaysScoops + 5, column=2).value = outputWorkSheet.cell(row=i, column=2).value
        outputWorkSheet.cell(row=i + todaysScoops + 5, column=3).value = outputWorkSheet.cell(row=i, column=3).value
        outputWorkSheet.cell(row=i + todaysScoops + 5, column=4).value = outputWorkSheet.cell(row=i, column=4).value
        outputWorkSheet.cell(row=i + todaysScoops + 5, column=5).value = outputWorkSheet.cell(row=i, column=5).value

    # Start writing last day's scoops
    out_row_index = 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Last Day\'s Scoops'
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=1, end_column=5)
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Date'
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Year'
    outputWorkSheet.cell(row=out_row_index, column=3).value = 'Time'
    outputWorkSheet.cell(row=out_row_index, column=4).value = 'Author'
    outputWorkSheet.column_dimensions['D'].width = 16
    outputWorkSheet.cell(row=out_row_index, column=5).value = 'Message'
    outputWorkSheet.column_dimensions['E'].width = 85
    out_row_index += 1
    # Search for last day's scoops in all scoops and copy if found
    for i in range(1 + todaysScoops + 5, scoopCounter + 1 + todaysScoops + 5):
        strCol1 = outputWorkSheet.cell(row=i, column=1).value
        strCol2 = outputWorkSheet.cell(row=i, column=2).value
        currentDate = ParseDate(strCol1, strCol2)
        if currentDate == lastDay:
            outputWorkSheet.cell(row=out_row_index, column=1).value = outputWorkSheet.cell(row=i, column=1).value
            outputWorkSheet.cell(row=out_row_index, column=2).value = outputWorkSheet.cell(row=i, column=2).value
            outputWorkSheet.cell(row=out_row_index, column=3).value = outputWorkSheet.cell(row=i, column=3).value
            outputWorkSheet.cell(row=out_row_index, column=4).value = outputWorkSheet.cell(row=i, column=4).value
            outputWorkSheet.cell(row=out_row_index, column=5).value = outputWorkSheet.cell(row=i, column=5).value
            outputWorkSheet.cell(row=out_row_index, column=5).style = outputWorkSheet.cell('A1').style.copy(
                alignment=Alignment(wrap_text=True))
            out_row_index += 1

    # Clear Last general scoop
    outputWorkSheet.cell(row=out_row_index, column=1).value = ''
    outputWorkSheet.cell(row=out_row_index, column=2).value = ''
    outputWorkSheet.cell(row=out_row_index, column=3).value = ''
    outputWorkSheet.cell(row=out_row_index, column=4).value = ''
    outputWorkSheet.cell(row=out_row_index, column=5).value = ''

    # Write 'All Scoops' header
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'All Scoops'
    outputWorkSheet.merge_cells(start_row=out_row_index, end_row=out_row_index, start_column=1, end_column=5)
    out_row_index += 1
    outputWorkSheet.cell(row=out_row_index, column=1).value = 'Date'
    outputWorkSheet.cell(row=out_row_index, column=2).value = 'Year'
    outputWorkSheet.cell(row=out_row_index, column=3).value = 'Time'
    outputWorkSheet.cell(row=out_row_index, column=4).value = 'Author'
    outputWorkSheet.cell(row=out_row_index, column=5).value = 'Message'


def CalculateDayOfTheWeek(currentDate):
    Jan12000 = 7  # Day of the week of begin reference date
    currentYear = int(currentDate[0:4])
    yearsElapsed = currentYear - 2000
    if yearsElapsed == 0:
        daysElapsed = 0
    else:
        daysElapsed = yearsElapsed * 365 + int((yearsElapsed - 1) / 4)
    currentMonth = int(currentDate[5:7])
    for i in range(1, currentMonth):
        daysElapsed += monthLengths[i]
    if currentYear % 4 == 0 and currentMonth > 2:
        daysElapsed += 1
    currentDay = int(currentDate[8:10])
    daysElapsed += currentDay
    dayOfTheWeek = ((daysElapsed + Jan12000) % 7)
    if dayOfTheWeek == 0:
        dayOfTheWeek = 7
    return dayOfTheWeek


def sort_key(input):
    return input[1] + CheckPrefixes(input[0], dictionary) + CheckPostfixes(input[0], dictionary)


def sort_key_today(input):
    return input[1] + CheckPrefixes(input[0], todayDictionary) + CheckPostfixes(input[0], todayDictionary)


def sort_key_msg(input):
    return input[1]


def sort_key_active(input):
    return input[1]


def IsYear(input):
    retValue = 0
    try:
        intinput = int(input)
        if intinput > 0:
            if (intinput / 10) > 0:
                if (intinput / 100) > 0:
                    if (intinput / 1000) > 0:
                        retValue = intinput
    except ValueError:
        retValue = 0
    return retValue


def CheckForScoopEmoji(input):
    retValue = 0
    for character in input:
        if ord(character) == 128308:
            retValue = 1
            break
    return retValue


def RemoveEmojis(input):
    strNew = ''
    counter = 0
    for character in input:
        if IsEmojiChar(character) == 1:
            counter += 1
        if IsLegalChar(character) == 1:
            strNew = strNew + character
    return strNew, counter


def IsLegalChar(character):
    retValue = 1
    order = ord(character)
    if order > 10000 or order == 8236 or order == 8234 or order == 11:
        retValue = 0
    return retValue


def IsEmojiChar(character):
    retValue = 0
    order = ord(character)
    if order >= 127744 and order <= 128755:
        retValue = 1
    if order >= 9728 and order <= 10175:
        retValue = 1
    return retValue


def CountWords(input, isHebrew):
    wordCount = -1
    iterator = 0
    index = re.split(' ', input, flags=re.IGNORECASE)
    reachedEnd = 0
    while reachedEnd == 0:
        try:
            if isHebrew == 0:
                legalWord = index[iterator] != '<Media' and index[iterator] != 'Omitted>' and index[
                                                                                                  iterator] != 'omitted>\n'
            else:
                legalWord = index[iterator] != '<\u05DE\u05D3\u05d9\u05d4' and index[
                                                                                   iterator] != '\u05d4\u05d5\u05e9\u05de\u05d8\u05d4>' and \
                            index[iterator] != '\u05d4\u05d5\u05e9\u05de\u05d8\u05d4>\n'
            if len(index[iterator]) > 1 and legalWord == True:
                cont = True
                if len(index[iterator]) == 2 and index[iterator][1] == '\n':
                    cont = False
                if cont:
                    try:
                        dictionary[index[iterator]] += 1
                    except KeyError:
                        dictionary[index[iterator]] = 1
                    try:
                        todayDictionary[index[iterator]] += 1
                    except KeyError:
                        todayDictionary[index[iterator]] = 1
                    wordCount += 1
            iterator += 1
        except IndexError:
            reachedEnd = 1
    iterator = 0
    index = re.split('\n', input, flags=re.IGNORECASE)
    reachedEnd = 0
    while reachedEnd == 0:
        try:
            if len(index[iterator]) > 0:
                wordCount += 1
            iterator += 1
        except IndexError:
            reachedEnd = 1
    if wordCount == -1:
        wordCount = 0
    foundMedia = re.search('<Media Omitted>', input, re.IGNORECASE)
    if foundMedia:
        wordCount = 0
    return wordCount


def CheckPrefixes(input, dictionaryToCheck):
    retValue = 0
    for prefix in prefixes:
        newinput = prefix + input
        try:
            retValue += dictionaryToCheck[newinput] + CheckPostfixes(newinput, dictionaryToCheck)
        except KeyError:
            retValue += CheckPostfixes(newinput, dictionaryToCheck)
    return retValue


def CheckPostfixes(input, dictionaryToCheck):
    retValue = 0
    for postfix in postfixes:
        newinput = input + postfix
        try:
            retValue += dictionaryToCheck[newinput]
        except KeyError:
            retValue += 0
    return retValue


def ParseDate(strCol1, strCol2):
    dateStr = str(strCol2) + '-'
    currentMonthNumber = '00'
    for month, englishMonth, monthNumber in englishMonths:
        if month == strCol1[0:3]:
            currentMonthNumber = monthNumber
    if len(currentMonthNumber) == 1:
        currentMonthNumber = '0' + currentMonthNumber
    dateStr += currentMonthNumber + '-'
    currentDayNumber = strCol1[4:]
    if len(currentDayNumber) == 1:
        currentDayNumber = '0' + currentDayNumber
    dateStr += currentDayNumber
    return dateStr


def IgnoreWord(input):
    retValue = 0
    for word in wordsToIgnore:
        appendedWord = word + '\n'
        if word == input or appendedWord == input:
            retValue = 1
            break
    return retValue


def IncrementDate(inputDate):
    year = inputDate[0:4]
    month = inputDate[5:7]
    day = inputDate[8:10]
    intday = int(day)
    intmonth = int(month)
    intyear = int(year)
    intday += 1
    if intday == 32:
        intday = 1
        intmonth += 1
        if intmonth == 13:
            intmonth = 1
            intyear += 1
    elif intday == 31:
        if intmonth == 4 or intmonth == 6 or intmonth == 9 or intmonth == 11:
            intday = 1
            intmonth += 1
    elif intday > 28:
        if intmonth == 2:
            intmonth = 3
            intday = 1
    if intmonth < 10:
        strmonth = '0' + str(intmonth)
    else:
        strmonth = str(intmonth)
    if intday < 10:
        strday = '0' + str(intday)
    else:
        strday = str(intday)
    return str(intyear) + '-' + strmonth + '-' + strday


if __name__ == "__main__":
    main()
