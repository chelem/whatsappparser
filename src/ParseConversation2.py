import argparse
import sys
import re
from datetime import datetime

from openpyxl import Workbook
from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import Font


def parse_arguments():
    def check_date(s):
        # Convert a date format to strptimeL
        # YYYY is %Y
        # YY is %y
        # MM is %m
        # DD is %d
        fmt = s.replace('YYYY', '%Y')
        fmt = fmt.replace('YY', '%y')
        fmt = fmt.replace('MM', '%m')
        fmt = fmt.replace('DD', '%d')

        # Everything else is passed to strptime as is
        return fmt

    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=str, help='Name of input txt file')
    parser.add_argument('output', type=str, help='Name of output Excel file')
    parser.add_argument('--date-format', type=check_date, help='Date format', default='MM/DD/YY', required=False)
    return parser.parse_args()


def extract_messages(filename, fmt):
    with open(filename, encoding="UTF-8") as f:
        lines = f.readlines()

    messages = []
    message = {'text': ""}

    for line in lines:
        # Example line: '4/3/17, 18:38 - Tal Elovits: וואי איזה פסיכיות. שידור ישיר?!'
        p = re.compile("(?P<date>.*?), (?P<time>\d{2}:\d{2}) - (?P<author>.*?): (?P<text>.*)")
        match = p.search(line)

        if match:
            if 'date' in message.keys():  # Check that message isn't just garbage text.
                messages.append(message)

            message = {'time': match.group('time'), 'author': match.group('author'), 'text': match.group('text')}

            datetime_object = datetime.strptime(match.group(1), fmt)

            # message['year'] = datetime_object.year

            message['date'] = datetime_object.strftime("%Y-%m-%d")

            message['wc'] = len(message['text'].split())

        else:
            message['text'] += line

    return messages

COLUMNS = [
    # In each brackets there are two word.
    #  The first is the name of the column in the excel file the script creates
    # The second is the field name in the dictionary of the messages
    ('Date', 'date'),
    ('Time', 'time'),
    ('Author', 'author'),
    ('Message', 'text'),
    ('Word Count', 'wc'),
]

def create_workbook():
    def get_header_cell(title):
        cell = WriteOnlyCell(ws, value=title)
        cell.font = Font(name='Calibri', size=12, bold=True)
        return cell

    wb = Workbook(write_only=True)
    ws = wb.create_sheet()

    # Add title row
    title_row = [get_header_cell(column[0]) for column in COLUMNS]
    ws.append(title_row)

    return wb, ws

def add_messages(ws, messages):
    def clean_controls(s):
        #This function removes unnecessary sign from each message
        result = ''

        for c in s:
            o = ord(c)
            if o >= 32 or o in (9, 10, 13):
                result += c
        return result

    def get_field(message, path):
        parts = path.split('.')
        obj = message

        for part in parts:
            try:
                obj = obj[part]
            except KeyError:
                print("Can't locate %s" % path)
                return ''

        s = str(obj)
        c = clean_controls(s)
        try:  # The date and time field coming from the txt file therefore they are string and they will be present as string in excel
             # we change the type of them to int to prevent it.

            c = int(c)
        except ValueError:
            pass
        return c

    count = 0
    for message in messages:
        fields = [get_field(message, column[1]) for column in COLUMNS]

        try:
            ws.append(fields)
            count += 1
            if count % 1000 == 0:  # Print a dot every 1000 messages
                print('.', end='')
                sys.stdout.flush()
        except Exception as e:
            # Report exceptions to the user but don't stop
            print(e, file=sys.stderr)

    print('')

def write_messages(messages, filename):
    wb, ws = create_workbook()
    add_messages(ws, messages)
    print("Performing final writing step")
    wb.save(filename)


def run():
    args = parse_arguments()
    print("Parsing file " + args.input)
    messages = extract_messages(args.input, args.date_format)
    print("Writing %d messages to %s" % (len(messages), args.output))
    write_messages(messages, args.output)
    print("Done")

if __name__ == '__main__':
    run()
